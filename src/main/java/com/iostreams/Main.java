package com.iostreams;

import com.iostreams.services.FileServices;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

public class Main {

    public static void main(String[] args) {
        FileServices fileServices = new FileServices(new BufferedReaderWrapper(), new FileWriterWrapper());
        String[] fileNames = {"DataCache\\old_file_1.txt",
        "DataCache\\old_file_2.txt",
        "DataCache\\old_file_3.txt"};
        //System.out.println(fileServices.readFiles(fileNames));

//        String str1 = "hhhhhhhhhh\nhhhhhhhhhhhhhhhhhhhhhh\nhhhhhhhhhhhhhhhhhhh";
//        String str2 = "jjjjjjjjjjj\njjjjjjjjjjjjjjjjjjjjjjj\njjjjjjjjjjjjjjjjj";
//        String str3 = "yyyyyyyyyyy\nyyyyyyyyyyyyyyyyyyyyyyyy\nyyyyyyyyyyyyyyyy";
//
//        fileServices.writeFileWithDelete(str1, "DataCache\\new_file_1.txt");
//        fileServices.writeFileToStart(str2, "DataCache\\new_file_1.txt");
//        fileServices.writeFileToEnd(str3, "DataCache\\new_file_1.txt");

        String name = "DataCache/new_file_2.txt";

        String[] n = {name};

        fileServices.readFiles(n);
    }
}
