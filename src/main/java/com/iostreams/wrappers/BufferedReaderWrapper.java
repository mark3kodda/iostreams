package com.iostreams.wrappers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class BufferedReaderWrapper {
    public BufferedReader getBufferedReader(File file) throws FileNotFoundException {
        return new BufferedReader(new FileReader(file));
    }
}